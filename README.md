# README #

Come parte del tutorial su 3D Slicer, a partire dal dataset VIX2 scaricabile da:

http://www.imagenglab.com/didattica/chirurgia_assistita@ingegneria/aa14_15/vix2.zip

Ogni studente dovrà segmentare il calcagno del piede destro (avendo come riferimento la propria vista , ovvero alla destra dell'osservatore)

successivamente caricare la labelmap della segmentazione facendo un git commit, avendo magari cura di fare un git pull subito prima del commit (o se preferite facendo un proprio branch).

**IMPORTANTE: IL FILE .NRRD CHE CARICATE DEVE AVERE COME NOME DEL FILE LA MATRICOLA , ALRTIMENTI SOVRASCRIVERETE A VICENDA I VOSTRI FILE**